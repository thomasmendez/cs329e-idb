# beginning of models.py
# note that at this point you should have created "bookdb" database (see install_postgres.txt).
from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy
import os
#import pymysql
#pymysql.install_as_MySQLdb()
#import MySQLdb

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",'postgres://postgres:asd123@localhost:5432/nobeldb2')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)


class Cat(db.Model):
    __tablename__ = 'categories'
    #idN = db.Column(db.Integer, primary_key = True)
    category = db.Column(db.String(50), primary_key = True)
    #laureates = db.relationship('Laureates', backref='cat', lazy= True)
    motivations = db.Column(db.String(40000), nullable = True)
    countries = db.Column(db.String(10000), nullable = True)
    firstYear = db.Column(db.Integer, nullable = True)
    lastYear = db.Column(db.Integer, nullable = True)
    laureates = db.relationship('Laureates', backref='Laureates1', lazy=True)
    def __repr__(self):
    	return self.category


class Countries(db.Model):
	__tablename__='countries'
	code = db.Column(db.String(2), primary_key = True)
	country_names = db.Column(db.String(20000))
	#laureates = db.Column(db.String(30000))
	categories= db.Column(db.String(20000))
	years = db.Column(db.String(30000))
	cities = db.Column(db.String(30000))
	count = db.Column(db.Integer)
	laureates = db.relationship('Laureates', backref='Laureates2', lazy=True)
	image = db.Column(db.String(30000))
	def __repr__(self):
		return self.code

class Laureates(db.Model):
	__tablename__='laureates'
	id = db.Column(db.Integer, primary_key = True)
	firstname = db.Column(db.String(500))
	surname = db.Column(db.String(500))
	born = db.Column(db.DATE)
	died = db.Column(db.DATE)
	bornCountry = db.Column(db.String(500))
	bornCountryCode = db.Column(db.String(500), db.ForeignKey('countries.code'))
	bornCity = db.Column(db.String(500))
	diedCountry = db.Column(db.String(500))
	diedCountryCode = db.Column(db.String(2))
	diedCity = db.Column(db.String(500))
	gender = db.Column(db.String(6))
	prizesYear = db.Column(db.Integer)
	prizesCategory = db.Column(db.String(50), db.ForeignKey('categories.category'))
	prizesShare = db.Column(db.Integer)
	prizesMotivation = db.Column(db.String(500))
	prizesAffiliationName = db.Column(db.String(500))
	prizesAffiliationCity = db.Column(db.String(500))
	prizesAffiliationCountry =  db.Column(db.String(500))
	def __repr__(self):
		return self.firstname + " " + self.surname

##db.drop_all()
db.create_all()
# End of models.py

#db = MYSQLdb.connect("localhost","nsusanward", "Ichigo=15", "nobeldb")

#cursor= db.cursor()

#result = cursor.fetchall()

#for i in range(len(result)):
#	print(result[i])
