import json
#from models import db, Categories, Countries, Laureates
from models import db, Cat
from models import db, Countries
from models import db, Laureates

def load_json(filename):
	with open(filename) as file:
		jsn = json.load(file)
		file.close
	return jsn

def create_cat():
	categories = load_json('./static/updated_json/categories.json')
	for n in categories["categories"]:
		category = n["category"]
		years = n["years"]
		firstYear = 1901
		lastYear = 2018
		motivations = ""
		countries_array = []
		for i in years:
			yearMotivations = i["motivations"]
			for j in yearMotivations:
				motivations += (str(j) + "\n")
			curCountries = i["countries"]
			for countr in curCountries:
				if countr not in countries_array:
					countries_array.append(countr)
		countries = ""
		for j in countries_array:
			countries += j[0] + " "
		newCategory = Cat(category = category, motivations = motivations, countries = countries,
			firstYear = firstYear, lastYear = lastYear)
		#(newCategory)
		db.session.add(newCategory)
		db.session.commit()
#create_cat()

def create_ctr():
	ctr = load_json('./static/updated_json/countries.json')
	for n in ctr["countries"]:
	    #create all of the blank variables for the countries
		country_names = ""
		years = ""
		cities = ""
		code = n["code"]
		categories = ""
		curCountryNames = n["country_names"]
		#go through the country names adding a number before each
		for i in curCountryNames:
			country_names += i + " "
		curYears = n["years"]
		#go through the years names adding a number before each
		for i in curYears:
			years += i + " "
		curCities = n["cities"]
		#go through the cities names adding a number before each
		for i in curCities:
			cities += i + " "
		curCategories = n["categories"]
		#go through the categories names adding a number before each
		for i in curCategories:
			categories += i + " "
		count = n["count"]
		image = n["image"]
		#create the newCountry vairable with the modified countries, cities, and categories
		newCountry = Countries(code= code, country_names = country_names,
			years = years, cities = cities, categories = categories, count = count, image = image)
		db.session.add(newCountry)
		db.session.commit()
#create_ctr()

def create_laureates():
	laureates = load_json('./static/updated_json/laureates.json')
	for n in laureates["laureates"]:
		id = n["id"]
		firstname = n["firstname"]
		if "surname" in n:
			surname = n["surname"]
		else:
			surname = firstname
			firstname = ""
		born = n["born"]
		if born == "0000-00-00":
			born="2100-01-01"
		died = n["died"]
		if died == "0000-00-00":
			died = "2100-01-01"
		if "bornCountry" in n:
			bornCountry = n["bornCountry"]
			bornCountryCode = n["bornCountryCode"]
			bornCity = n["bornCity"]
		if "diedCountry" in n:
			diedCountry = n["diedCountry"]
		if "diedCountryCode" in n:
			diedCountryCode = n["diedCountryCode"]
		if "diedCity" in n:
			diedCity = n["diedCity"]
		gender = n["gender"]
		prizes = n["prizes"]
		for i in range(len(n["prizes"])):
			curYear = prizes[i]
			yearPrize = curYear["year"]
			category = curYear["category"]
			share = curYear["share"]
			motivation = curYear["motivation"]
			affiliations = curYear["affiliations"]
			#print("affiliations is", affiliations)
			#print("type affiliation is", type(affiliations))
			if (len(affiliations[0]) != 0):
				for j in affiliations:
					if "name" in j:
						affName = j["name"]
					if "city" in j:
						affCity = j["city"]
					if "country" in j:
						affCountry = j["country"]
		newLaureate = Laureates(id = id, firstname = firstname, surname = surname, born = born,
	died = died, bornCountry = bornCountry, bornCountryCode = bornCountryCode, bornCity = bornCity,
	diedCountry = diedCountry, diedCountryCode = diedCountryCode, diedCity = diedCity, gender = gender,
	prizesYear = yearPrize, prizesCategory = category, prizesShare = share, prizesMotivation = motivation,
	prizesAffiliationName = affName, prizesAffiliationCity = affCity, prizesAffiliationCountry =affCountry)
		db.session.add(newLaureate)
		db.session.commit()
#create_laureates()

create_ctr()
create_cat()
create_laureates()