import os
import sys
import unittest
from models import db, Cat, Countries, Laureates
from create_db import load_json, create_cat, create_ctr, create_laureates

class DBTestCases(unittest.TestCase):

    def test_source_insert_1(self):
        ##insert Physics instance in Categories & check existence
        s = Cat(category = 'Physics', firstYear = 1901 )
        db.session.add(s)
        db.session.commit()
        r = db.session.query(Cat).filter_by(category = 'Physics').one()
        
        self.assertEqual(int(r.firstYear), 1901)
        db.session.query(Cat).filter_by(category='Physics').delete()
        db.session.commit()

    def test_existence_2(self):

    	##Whether Australia is a country that has won a prize
    	r = db.session.query(Countries).filter_by(code = 'AU').one()

    	self.assertEqual(str(r.country_names), 'Australia ')

    def test_existence_3(self):

    	##Test whether Wilhelm Conrad is a Laureate
    	r = db.session.query(Laureates).filter_by(id = '1').one()

    	self.assertEqual(str(r.firstname), 'Wilhelm Conrad')
    	
    	
    	
    def test_source_query_count_4(self):
		##number of laureates born in Germany (country code == 'DE')

    	r = db.session.query(Laureates).filter(Laureates.bornCountryCode=='DE').all()

    	self.assertEqual(len(r), 82)

    def test_source_query_count_5(self):
		##number of laureates in 2018

    	r = db.session.query(Laureates).filter(Laureates.prizesYear==2018).all()

    	self.assertEqual(len(r), 12)

    def test_source_query_count_6(self):
		##number of laureates who won in the Physics category

    	r = db.session.query(Laureates).filter(Laureates.prizesCategory=='physics').all()

    	self.assertEqual(len(r), 208)
    	
    	

if __name__ == '__main__':
    unittest.main()