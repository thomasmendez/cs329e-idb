#-----------------------------------------
# main.py
# creating first flask application
#-----------------------------------------
from flask import Flask, render_template
#from models import app, db, Categories, Laureates, Countries
from models import app, db, Cat, Laureates, Countries
from create_db import create_cat, create_ctr, create_laureates
import subprocess

# app = Flask(__name__)

@app.route('/')
def splash():
    return render_template('splash.html')

@app.route('/about/')
def about():
    return render_template('about.html')

@app.route('/test/')
def test():
    p = subprocess.Popen(["coverage", "run", "--branch", "test.py"],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            stdin=subprocess.PIPE)
    out, err = p.communicate()
    output=err+out
    output = output.decode("utf-8") #convert from byte type to string type
    
    return render_template('test.html', output = " ".join(output.split("\n")))

#dynamic country pillar
@app.route('/country/')
def country():
    countries = db.session.query(Laureates).distinct(Laureates.bornCountryCode)
    return render_template('dynamic_country.html', countries=countries)

#dynamic country instance
@app.route('/country/<string:code>/<string:title>/')
def dcountry(code, title):
    country = db.session.query(Laureates).filter_by(bornCountryCode = code)
    cats = db.session.query(Laureates).filter_by(bornCountryCode = code).distinct(Laureates.prizesCategory)
    years = db.session.query(Laureates).filter_by(bornCountryCode = code).distinct(Laureates.prizesYear)
    cities = db.session.query(Laureates).filter_by(bornCountryCode = code).distinct(Laureates.bornCity)
    return render_template('country_instance.html', years=years, cities=cities, cats=cats, country = country, title=title, code=code, num=country.count())

#dynamic category pillar
@app.route('/category/')
def category():
    categories = db.session.query(Laureates).distinct(Laureates.prizesCategory)
    return render_template('dynamic_category.html', categories = categories)

#dynamic category instance
@app.route('/category/<string:category>/')
def dcategory(category):
    categories = db.session.query(Laureates).filter_by(prizesCategory = category).order_by(Laureates.prizesYear.desc())
    return render_template('category_instance.html', categories = categories, title=category)

#dynamic laureates pillar
@app.route('/laureate/')
def laureate():
    laureates = db.session.query(Laureates).order_by(Laureates.surname.asc()).all()
    return render_template('dynamic_laureate.html', laureates = laureates)

#dyanmic laureate instance
#to be updated
@app.route('/laureate/<id>/')
def dlaureate(id):
    laureate = db.session.query(Laureates).filter_by(id = id)
    return render_template('laureate_instance.html', laureate = laureate)

if __name__ == "__main__":
    app.run()

#----------------------------------------
# end of main.py
#-----------------------------------------
