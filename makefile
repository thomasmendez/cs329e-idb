FILES :=                            \
    main.py							\
	create_db.py                    \
	models.py                       \
	test.py							\
	models.html						\
	requirements.txt				\
	IDB3.log						\


#    collatz-tests/YourGitLabID-RunCollatz.in   \
#    collatz-tests/YourGitLabID-RunCollatz.out  \
#    collatz-tests/YourGitLabID-TestCollatz.out \
#    collatz-tests/YourGitLabID-TestCollatz.py  \
#

ifeq ($(shell uname), Darwin)          # Apple
    PYTHON   := python3.5
    PIP      := pip3.5
    PYLINT   := pylint
    COVERAGE := coverage-3.5
    PYDOC    := pydoc3.5
    AUTOPEP8 := autopep8
else ifeq ($(CI), true)                # Travis CI
    PYTHON   := python
    PIP      := pip3.5
    PYLINT   := pylint
    COVERAGE := coverage
    PYDOC    := pydoc
    AUTOPEP8 := autopep8
else ifeq ($(shell uname -p), unknown) # Docker
    PYTHON   := python                # on my machine it's python
    PIP      := pip3.5
    PYLINT   := pylint
    COVERAGE := coverage-3.5
    PYDOC    := python -m pydoc        # on my machine it's pydoc 
    AUTOPEP8 := autopep8
else                                   # UTCS
    PYTHON   := python3.5
    PIP      := pip3
    PYLINT   := pylint3
    COVERAGE := coverage-3.5
    PYDOC    := pydoc3.5
    AUTOPEP8 := autopep8
endif


test.py:
	$(PYTHON) test.py

models.html: models.py
	$(PYDOC) -w models

IDB3.log:
	git log > IDB3.log

#RunCollatz.tmp: RunCollatz.in RunCollatz.out RunCollatz.py
#	$(PYTHON) RunCollatz.py < RunCollatz.in > RunCollatz.tmp
#	diff RunCollatz.tmp RunCollatz.out

#TestCollatz.tmp: TestCollatz.py
#	$(COVERAGE) run    --branch TestCollatz.py >  TestCollatz.tmp 2>&1
#	$(COVERAGE) report -m                      >> TestCollatz.tmp
#	cat TestCollatz.tmp

check:
	@not_found=0;                                 \
    for i in $(FILES);                            \
    do                                            \
        if [ -e $$i ];                            \
        then                                      \
            echo "$$i found";                     \
        else                                      \
            echo "$$i NOT FOUND";                 \
            not_found=`expr "$$not_found" + "1"`; \
        fi                                        \
    done;                                         \
    if [ $$not_found -ne 0 ];                     \
    then                                          \
        echo "$$not_found failures";              \
        exit 1;                                   \
    fi;                                           \
    echo "success";

clean:
	rm -f IDB3.log

config:
	git config -l

format:
	$(AUTOPEP8) -i models.py
	

scrub:
	make clean
	rm -f  models.html
	rm -f  models.log
	

status:
	make clean
	@echo
	git branch
	git remote -v
	git status
	
versions:
	which     $(AUTOPEP8)
	autopep8 --version
	@echo
	which    $(COVERAGE)
	coverage --version
	@echo
	which    git
	git      --version
	@echo
	which    make
	make     --version
	@echo
	which    $(PIP)
	pip      --version
	@echo
#	which    $(PYDOC)
#	pydoc    --version
#	@echo
	which    $(PYLINT)
	pylint   --version
	@echo
	which    $(PYTHON)
	python   --version

test: test.py 

check: check